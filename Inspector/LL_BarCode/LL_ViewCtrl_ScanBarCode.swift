//  Copyright © 2017 Lumen Labs. All rights reserved.



import UIKit
import AVFoundation

// Usage:
//          Scan the bar code.
//
// Remarks:
//          Note: add a NSCameraUsageDescription to Info.plist
//          Refer to: http://www.hangge.com/blog/cache/detail_950.html
//
class LL_ViewCtrl_ScanBarCode : UIViewController, AVCaptureMetadataOutputObjectsDelegate,
UIAlertViewDelegate{
    
    var scanRectView:UIView!
    var device:AVCaptureDevice!
    var input:AVCaptureDeviceInput!
    var output:AVCaptureMetadataOutput!
    var session:AVCaptureSession!
    var preview:AVCaptureVideoPreviewLayer!
    
    
    // a timer for periodic things:
    fileprivate var timer: Timer?; let TIMER_PERIOD = 1 // unit: s
    @objc func timer_isr() { //print("timer_isr() is called.")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        fromCamera()
    }
    override func viewWillAppear(_ animated: Bool) { super.viewWillAppear(animated); timer = Timer.scheduledTimer(timeInterval: TimeInterval(TIMER_PERIOD), target: self, selector:#selector(timer_isr), userInfo: nil, repeats: true)
        // Xiong: turn on the torch, see: https://stackoverflow.com/questions/27207278/how-to-turn-flashlight-on-and-off-in-swift
        if (device.hasTorch) {
            do {
                try device.lockForConfiguration()
                    try device.setTorchModeOn(level: 1.0)
                device.unlockForConfiguration()
            } catch { print(error) }
        }
    }
    override func viewDidDisappear(_ animated: Bool) { super.viewDidDisappear(animated); timer?.invalidate()
        // Xiong: turn off the torch, see: https://stackoverflow.com/questions/27207278/how-to-turn-flashlight-on-and-off-in-swift
        if (device.hasTorch) {
            do {
                try device.lockForConfiguration()
                    device.torchMode = AVCaptureDevice.TorchMode.off
                device.unlockForConfiguration()
            } catch { print(error) }
        }
    }

    //通过摄像头扫描
    func fromCamera() {
        do{
            self.device = AVCaptureDevice.default(for: AVMediaType.video)
            
            self.input = try AVCaptureDeviceInput(device: device)
            
            self.output = AVCaptureMetadataOutput()
            output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            
            self.session = AVCaptureSession()
            if UIScreen.main.bounds.size.height<500 {
                self.session.sessionPreset = AVCaptureSession.Preset.vga640x480
            }else{
                self.session.sessionPreset = AVCaptureSession.Preset.high
            }
            
            self.session.addInput(self.input)
            self.session.addOutput(self.output)
            
//            self.output.metadataObjectTypes = [AVMetadataObject.ObjectType.ean13,
//                                               AVMetadataObject.ObjectType.ean8, AVMetadataObject.ObjectType.code128,
//                                               AVMetadataObject.ObjectType.code39,AVMetadataObject.ObjectType.code93]
            self.output.metadataObjectTypes = self.output.availableMetadataObjectTypes
            
            //计算中间可探测区域
            let windowSize = UIScreen.main.bounds.size;
            let scanSize = CGSize(width:windowSize.width*3/4,
                                  height:windowSize.width*3/4)
            var scanRect = CGRect(x:(windowSize.width-scanSize.width)/2,
                                  y:(windowSize.height-scanSize.height)/2,
                                  width:scanSize.width, height:scanSize.height)
            //计算rectOfInterest 注意x,y交换位置
            scanRect = CGRect(x:scanRect.origin.y/windowSize.height,
                              y:scanRect.origin.x/windowSize.width,
                              width:scanRect.size.height/windowSize.height,
                              height:scanRect.size.width/windowSize.width)
            //设置可探测区域
            self.output.rectOfInterest = scanRect
            
            self.preview = AVCaptureVideoPreviewLayer(session:self.session)
            self.preview.videoGravity = AVLayerVideoGravity.resizeAspectFill
            self.preview.frame = UIScreen.main.bounds
            self.view.layer.insertSublayer(self.preview, at:0)
            
            //添加中间的探测区域绿框
            self.scanRectView = UIView();
            self.view.addSubview(self.scanRectView)
            self.scanRectView.frame = CGRect(x:0, y:0, width:scanSize.width,
                                             height:scanSize.height)
            self.scanRectView.center = CGPoint(x:UIScreen.main.bounds.midX,
                                               y:UIScreen.main.bounds.midY)
            self.scanRectView.layer.borderColor = UIColor.green.cgColor
            self.scanRectView.layer.borderWidth = 1
            
            //开始捕获
            self.session.startRunning()
            
//            //放大
//            do {
//                try self.device!.lockForConfiguration()
//            } catch _ {
//                NSLog("Error: lockForConfiguration.");
//            }
//            self.device!.videoZoomFactor = 1.5
//            self.device!.unlockForConfiguration()
            
        }catch _ as NSError{
            //打印错误消息
            let alertController = UIAlertController(title: "提醒",
                                                    message: "请在iPhone的\"设置-隐私-相机\"选项中,允许本程序访问您的相机",
                                                    preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "确定", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    //摄像头捕获
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        guard metadataObjects.count > 0 else { return }
        if let metadataObject = metadataObjects[0] as? AVMetadataMachineReadableCodeObject {
            if let stringValue = metadataObject.stringValue {
                if stringValue.count == 14 {
                    self.session.stopRunning()
    //                // output
    //                let alertController = UIAlertController(title: "二维码", message: stringValue,preferredStyle: .alert)
    //                let okAction = UIAlertAction(title: "确定", style: .default, handler: { action in
    //                    self.session.startRunning() // scan again
    //                })
    //                alertController.addAction(okAction)
    //                self.present(alertController, animated: true, completion: nil)
                    LL_Inspector.shared.sn = stringValue
                    navigationController?.popViewController(animated: true)//dismiss(animated: false, completion: nil)
                } else { print("LL_ERR: the bar code object is not our Lumos!") }
            } else { print("LL_ERR: the bar code object has no code!") }
        } else { print("LL_ERR: it's not a bar code object!") }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


