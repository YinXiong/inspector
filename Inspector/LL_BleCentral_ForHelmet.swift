//  Copyright © 2017 Lumen Labs. All rights reserved.



import UIKit
import CoreBluetooth



// Usage:
//          Call "scan()" to launch the BLE scan for helmets.
//          Check the state by "central_manager.state", or put your own code in the CBCentralManagerDelegate func.
//          Use "helmets_connected".
//          Set is_pairing_mode to true to connect to any pairing helmet.
//
// Remarks:
//          The number of helmets supported varies in different iOS devices.
//          About "Why disconnect": http://yaunch.io/corebluetooth-gracefully-handle-disconnects/
//          Pls check the BLE parameter of firmware as Apple required: https://developer.apple.com/library/content/qa/qa1931/_index.html
//
class LL_BleCentral_ForHelmet: NSObject { static let shared = LL_BleCentral_ForHelmet();
    public var central_manager : CBCentralManager!
    public var is_pairing_mode = false
    public var helmets_connected = [LL_BlePeripheral_Helmet]()

    fileprivate var temp_used_for_referring_the_peripheral_before_connecting : CBPeripheral! // an iOS bug?
    fileprivate var temp_adv_name_before_connect = "" // temp shared by delegate "didDiscover" and "didConnect"

    override init() { super.init();
        central_manager = CBCentralManager(delegate: self, queue: nil)
        helmets_connected.removeAll()
    }
    
    public func recover_after_DFU() {
        helmets_connected.removeAll()
        scan()
    }
    
    public func scan() { // if(.poweredOn != central_manager.state) { print("BLE does not power on!"); return; }
        guard central_manager.state == .poweredOn else { print("LL_ERR: BLE is not power on yet!"); return }
        central_manager.delegate = self // always recover the delegate, which might be modified by DFU or others.
        central_manager.scanForPeripherals(withServices: [CBUUID(string:BLE_UUID_SRV_DIS), CBUUID(string:BLE_UUID_SRV_BATT)], options:[CBCentralManagerScanOptionAllowDuplicatesKey: false])  // false: coalesce the same peripheral, not interrupt my application routine too often.
    }
    
    public func main_loop() {
        for helmet in helmets_connected {
            helmet.main_loop()
        }
    }
}

extension LL_BleCentral_ForHelmet : CBCentralManagerDelegate {
    func centralManagerDidUpdateState(_ central: CBCentralManager) { // print("centralManagerDidUpdateState: \(central.state)")
        switch central.state {
        case .unknown:      break;
        case .resetting:    break;
        case .unsupported:  break;
        case .unauthorized: break;
        case .poweredOff:   break;
        case .poweredOn:    scan(); break;
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) { //print("centralManager.didDiscover!")
        print("CBAdvertisementDataLocalNameKey: \(String(describing: advertisementData[CBAdvertisementDataLocalNameKey]))")
        
        // whether adv name exist
        if let adv_name = advertisementData[CBAdvertisementDataLocalNameKey] as? String {
            temp_adv_name_before_connect = adv_name
        } else { return }
        
        // whether adv name valid
        guard ((temp_adv_name_before_connect == BLE_ADV_NAME_OF_HELMET_LUMOS) || (temp_adv_name_before_connect == BLE_ADV_NAME_OF_HELMET_LITE)) else { print("LL_MSG: Not a Lumos/Lite!"); return; }
        
        temp_used_for_referring_the_peripheral_before_connecting = peripheral
        // try to connect to a paired helmet
//        for helmet_id in LL_Data_Persistent.shared.helmets_id {
//            if peripheral.identifier.uuidString == helmet_id { print("LL_MSG: found a paired helmet!")
//                // try to connect even when helmet is paring, because the paring maybe for remote.
//                central_manager.stopScan(); central_manager.connect(peripheral, options: nil);
//                return;
//            }
//        }
        // try to connect to any pairing helmet
        if LL_Inspector.shared.state == .WAIT_HELMET { //if is_pairing_mode {
            if let manufacturer_data = advertisementData[CBAdvertisementDataManufacturerDataKey] as? Data {
                let helmet_paring_status = manufacturer_data.u8; if HELMET_PAIRING_STATUS_YES == helmet_paring_status { print("LL_MSG: found a helmet in paring mode, connecting!")
                    if helmets_connected.count == 0 { // only one
                        LL_Inspector.shared.rssi = RSSI.intValue
                        central_manager.stopScan(); central_manager.connect(peripheral, options: nil); is_pairing_mode = false;
                    }
                } else { print("LL_MSG: the helmet is not in paring mode!") }
            } else { print("LL_ERR: the helmet has no Manufacturer Data!") }
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) { //print("centralManager.didConnect!");
//        var helmet_type = DEVICE_TYPE.UNKNOWN
//        if(      temp_adv_name_before_connect == BLE_ADV_NAME_OF_HELMET_LUMOS ) { helmet_type = .HELMET_KICKSTARTER; }
//        else if( temp_adv_name_before_connect == BLE_ADV_NAME_OF_HELMET_LITE  ) { helmet_type = .HELMET_LITE;        }
//        else { print("LL_ERR: didConnect but temp_adv_name_before_connect is neither Lumos nor Lite!"); print(temp_adv_name_before_connect); // should never happen
//            // should never happen but still happend several times!
//            central_manager.cancelPeripheralConnection(peripheral);
//            if(LL_Data_Persistent.shared.helmets_id.count == 0) { is_pairing_mode = true; } // need recover the pairing if no helmet paired yet!
//            scan(); return;
//        }
//
        let helmet = LL_BlePeripheral_Helmet(); helmet.peripheral = peripheral; helmet.peripheral.delegate = helmet; //print("LL_MSG: \(String(describing: peripheral.name)) connected!")
//        LL_Data_Persistent.shared.helmet_load(helmet_id: helmet.peripheral.identifier.uuidString, to: helmet); helmet.helmet_type = helmet_type
        helmets_connected.append(helmet)
        
//        // save the peripheral
//        if LL_Data_Persistent.shared.helmets_id.contains(helmet.peripheral.identifier.uuidString) == false {
//            LL_Data_Persistent.shared.helmets_id.append( helmet.peripheral.identifier.uuidString)
//        }
//
//        // Notification to watch
//        LL_ConnectionWithWatch.share.sendMessageToWatch(key: LL_PHONE_DETECTED_KEY, value: LL_PHONE_DETECTED_HELMET_ON);
//        // Auto-Tracking
//        if((1 == LL_Data_Persistent.shared.auto_tracking) && (("" !=  LL_Account.shared.current_user_name)) ) {
//            _ = LL_Tracer.sharedInstance.start(LL_Account.shared.current_user_id)
//            // No need pop because LL_ViewCtrl_MainTabBar is always working already! //LL_UI_pop_all_app_tabs_to_root() // let LL_ViewCtrl_MainTabBar popup Auto-Tracking
//        }
//
        // discover service
        peripheral.discoverServices([CBUUID(string:BLE_UUID_SRV_DIS), CBUUID(string:BLE_UUID_SRV_BATT), CBUUID(string:BLE_UUID_SRV_UART)])
//
//        if(helmets_connected.count < LL_Data_Persistent.shared.helmets_id.count) { scan() }
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) { print("centralManager.didFailToConnect: \(String(describing: error))");
        scan() // try again
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) { print("centralManager.didDisconnectPeripheral: \(String(describing: error))");
        for helmet in helmets_connected {
            if(helmet.peripheral == peripheral) {
//              helmet.battery_notifyWhenDisconnected()
//              LL_Data_Persistent.shared.helmet_store(helmet_id: helmet.peripheral.identifier.uuidString, from: helmet)
                helmets_connected = helmets_connected.filter({ $0 != helmet })
                break
            }
        }
        
//
//        // forget the helmet if kicked out by it, because it "kick out" app only when it has paired with another new user.
//        if let ns_error = error as NSError? {
//            if ns_error.code == 7 { // kicked out by helmet
//                // forget the helmet
//                if let index = LL_Data_Persistent.shared.helmets_id.index(of: peripheral.identifier.uuidString) {
//                    LL_Data_Persistent.shared.helmets_id.remove(at: index)
//                }
//            }
//        }
//
//        // Notification to watch
//        LL_ConnectionWithWatch.share.sendMessageToWatch(key: LL_PHONE_DETECTED_KEY, value: LL_PHONE_DETECTED_HELMET_OFF);
//        // Auto-Tracking
//        if((1 == LL_Data_Persistent.shared.auto_tracking) && (("" !=  LL_Account.shared.current_user_name)) ) {
//            _ = LL_Tracer.sharedInstance.stop()
//            let result_of_saving = LL_Tracer.sharedInstance.save()
//            switch result_of_saving {
//            case .SAVED: break
////            case .ERR_LOCATIONS_NOT_ENOUGH  : LL_Notification.shared.schedule(Date(), "Auto-Tracking", "No enough locations!"); break;
////            case .ERR_DURATION_NOT_ENOUGH   : LL_Notification.shared.schedule(Date(), "Auto-Tracking", "No enough duration!"); break;
////            case .ERR_DISTANCE_NOT_ENOUGH   : LL_Notification.shared.schedule(Date(), "Auto-Tracking", "No enough distance!"); break;
////            case .ERR_AVG_SPEED_IS_0        : LL_Notification.shared.schedule(Date(), "Auto-Tracking", "The average speed is 0!"); break;
////            case .ERR_SAVE_FILE_FAILED      : LL_Notification.shared.schedule(Date(), "Auto-Tracking", "Failed when saving data.(-F)"); break;
////            case .ERR_SAVE_DB_FAILED        : LL_Notification.shared.schedule(Date(), "Auto-Tracking", "Failed when saving data.(-S)"); break;
//            default: break
//            }
//            LL_Location.shared.location_manager.stopUpdatingLocation()
//        }

        scan() // try again
    }
}



