//  Copyright © 2017 Lumen Labs. All rights reserved.



import Foundation
import CoreBluetooth
//import iOSDFULibrary    // Nordic DFU



// Usage:
//          None.
//
// Remarks:
//          None.
//
class LL_BlePeripheral_Helmet: NSObject { //static let shared = LL_BleCentral_ForHelmet();
    override init() { super.init();
    }

    public var peripheral : CBPeripheral!

    /////////////////////////////////////// helmet settings ///////////////////////////////////////
    // helmet type
    var helmet_type = DEVICE_TYPE.UNKNOWN // selected by user after app installed, then corrected by the 1st connection if user is wrong.
    // flash mode
    var flash_mode_front_light  = 0
    var flash_mode_rear_light   = 0
    var flash_mode_turn_signals = 0
    // battery notification (see LL_BatteryNotificationSetting)
//  var battery_notification = 3
    // beeping setting (see LL_CMD_BEEPING_SETTINGS)
    var beeping_setting = 3
    // brake function
    var is_brake_function_on = false
    var brake_function_sensitivity = Float(0.5)
    let BRAKE_SENSE_UPPER = Float(5000) // 5000: provided by firmeware
    let BRAKE_SENSE_LOWER = Float(2000) // 2000: provided by firmeware
    public func convertBrakeSensitivityToSliderValue(_ val: String) -> Float {
        guard let floatVal = Float(val), floatVal <= BRAKE_SENSE_UPPER && floatVal >= BRAKE_SENSE_LOWER else { return 0.5 }
        return round(100 * ((BRAKE_SENSE_UPPER - floatVal) / (BRAKE_SENSE_UPPER - BRAKE_SENSE_LOWER)))/100
    }
    public func convertSliderToBrakeSensitivityValue(_ val: Float) -> String {
        guard val <= 1.0 && val >= 0 else { return String(format: "%.0f", (BRAKE_SENSE_UPPER - (BRAKE_SENSE_UPPER - BRAKE_SENSE_LOWER)/2)) }
        return String(format: "%.0f", BRAKE_SENSE_UPPER - (BRAKE_SENSE_UPPER - BRAKE_SENSE_LOWER) * val)
    }
    /////////////////////////////////////// helmet settings ///////////////////////////////////////

    
    // BLE service: Divice Info Service
    fileprivate var srv_DIS: CBService?
    fileprivate var srv_DIS_chr_ver: CBCharacteristic?
    public var firmware_version = ""
    
    // BLE service: Battery Service
    fileprivate var srv_BATT: CBService?
    fileprivate var srv_BATT_chr_batt: CBCharacteristic?
    public var battery_percentage_helmet = -1.0
    public var battery_percentage_remote = -1.0
    fileprivate func round_battery_value_to_1to100(_ value_u8: UInt8) -> UInt8 { if(0 == value_u8) { return 1 } else if(100 < value_u8) { return 100 } else { return value_u8 } }
    
    // BLE service: UART Service
    fileprivate var srv_UART: CBService?
    fileprivate var srv_UART_chr_tx: CBCharacteristic?
    fileprivate var srv_UART_chr_rx: CBCharacteristic?
    // TX
    public var buf_to_helmet = [String](); fileprivate var is_tx_busy = false; fileprivate var tx_cnt = Date()
    fileprivate func send_to_helmet(_ string: String) {
//      guard helmet != nil else { return }
        guard srv_UART_chr_tx != nil else { return }
        let data = string.data(using: String.Encoding.utf8) ?? Data()
        peripheral.writeValue(data, for: srv_UART_chr_tx!, type:CBCharacteristicWriteType.withResponse); is_tx_busy = true
    }
    // RX
    fileprivate func parse(_ data: Data) {
        /*
        guard let dataString = String.init(data: data, encoding: String.Encoding.utf8) else { return }
        if dataString.hasPrefix(LL_CMD_RESPONSE_REMOTE_BATTERY_LEVEL) { //helmet.buf_to_helmet.append(LL_CMD_REQUEST_HELMET_BATTERY_LEVEL)
            let reading: String = dataString.substring(from: dataString.index(dataString.startIndex, offsetBy: LL_CMD_RESPONSE_REMOTE_BATTERY_LEVEL.count))
            if true == reading.contains(LL_REMOTE_BATTERY_NOT_AVAILABLE) {
                battery_percentage_remote = -1.0
            } else {
                battery_percentage_remote = Double(round_battery_value_to_1to100(UInt8(((reading as NSString).intValue)))) / 100.0
                if(battery_percentage_remote < 0.01) { battery_percentage_remote = -1.0 } // show "---" instead of "0%" when receiving the bug data "BL-R:000" from helmet.
            }
        } else if dataString.hasPrefix(LL_CMD_RESPONSE_HELMET_BATTERY_LEVEL) {
            let reading = dataString.substring(from: dataString.index(dataString.startIndex, offsetBy: LL_CMD_RESPONSE_HELMET_BATTERY_LEVEL.count))
            battery_percentage_helmet = Double(round_battery_value_to_1to100(UInt8((reading as NSString).intValue))) / 100.0
        } else if dataString.hasPrefix(LL_CMD_DISCONNECTING_HEADS_UP) {
            // What to do ???
        } else if dataString.hasPrefix(LL_CMD_SWITCH_OFF_BRAKE_FUNCTION) || dataString.hasPrefix(LL_CMD_SWITCH_ON_BRAKE_FUNCTION) {
            is_brake_function_on = dataString.substring(to: dataString.index(dataString.startIndex, offsetBy: LL_CMD_SWITCH_OFF_BRAKE_FUNCTION.count)) == LL_CMD_SWITCH_ON_BRAKE_FUNCTION
            brake_function_sensitivity = convertBrakeSensitivityToSliderValue(dataString.substring(from: dataString.index(dataString.startIndex, offsetBy: 4)))
        }
         */
        if data.count == 17 {
            LL_Inspector.shared.batt_helmet = Int(data.u8s[16])
            LL_Inspector.shared.firmware_version = data.u8s[8].description + "." + data.u8s[9].description
            buf_to_helmet.append("TPAIR")
        } else if data.count == 9 {
            LL_Inspector.shared.batt_remote = Int(data.u8s[8])
        }

    }

//    // BLE service: DFU Service
//    public var dfu_state = DFUState.completed; // is "completed" ok?
//    public var dfu_progress = 0;
//    func launch_DFU(with central_manager: CBCentralManager, file_path: String) {
//        if let zipFile = URL(string: file_path) {
//            guard let firmwareFile = DFUFirmware(urlToZipFile: zipFile) else { print("DFUFirmware fail!"); return; }
//            let initiator = DFUServiceInitiator(centralManager: central_manager, target: self.peripheral).with(firmware: firmwareFile);
//            initiator.delegate          = self;
//            initiator.progressDelegate  = self;
//            let _ = initiator.start(); print("DFU start!");
//        }
//    }
    
    // Battery Notification
    fileprivate var has_NotifyOnConnected_done = false
    public func battery_notifyWhenConnected() { guard false == has_NotifyOnConnected_done else { return }
        if( (battery_percentage_helmet > 0) || (battery_percentage_remote > 0) ) {
//            BatteryManager.sharedInstance.scheduleNotificationsForConnectionState(.OnConnect, helmet: self)
            has_NotifyOnConnected_done = true
        }
    }
    public func battery_notifyWhenDisconnected() {
//        BatteryManager.sharedInstance.scheduleNotificationsForConnectionState(.OnDisconnect, helmet: self)
    }
    
    public func main_loop() {
        // tx_loop
//        if(dfu_state == DFUState.completed) {
//            if Date().timeIntervalSince(tx_cnt) > TimeInterval(10) { tx_cnt = Date(); buf_to_helmet.append(LL_CMD_REQUEST_HELMET_BATTERY_LEVEL) } // 4: ask for battery level per 4s.
            guard let data = buf_to_helmet.first else { return }
            guard false == is_tx_busy else { return }
            send_to_helmet(data); print("========================write:\(data) to \(peripheral.identifier)")
//        }
//        // Battery Notification
//        battery_notifyWhenConnected()
    }

}

extension LL_BlePeripheral_Helmet: CBPeripheralDelegate {
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        guard nil == error else { print("LL_ERR: didDiscoverServices  \(String(describing: error?.localizedDescription))"); return; }
        guard nil != peripheral.services else { return }
        for service in peripheral.services! {
            if (service.characteristics == nil) { // only re-discover if hasn't discovered yet
                switch service.uuid {
                case CBUUID(string:BLE_UUID_SRV_DIS):
                    srv_DIS = service
                    peripheral.discoverCharacteristics([CBUUID(string:BLE_UUID_SRV_DIS_CHR_VER)], for: service)
                    break;
                case CBUUID(string:BLE_UUID_SRV_BATT):
                    srv_BATT = service
                    peripheral.discoverCharacteristics([CBUUID(string:BLE_UUID_SRV_BATT_CHR_LVL)], for: service)
                    break;
                case CBUUID(string:BLE_UUID_SRV_UART):
                    srv_UART = service
                    peripheral.discoverCharacteristics([CBUUID(string:BLE_UUID_SRV_UART_CHR_TX),
                                                        CBUUID(string:BLE_UUID_SRV_UART_CHR_RX)], for: service)
                    break;
                default: peripheral.discoverCharacteristics(nil, for: service); break;
                }
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        guard nil == error else { print("LL_ERR: didDiscoverCharacteristicsFor  \(String(describing: error?.localizedDescription))"); return; }
        guard nil != service.characteristics else { return }
        for characteristic in service.characteristics! {
            switch characteristic.uuid {
            case CBUUID(string:BLE_UUID_SRV_DIS_CHR_VER):
                srv_DIS_chr_ver = characteristic
                peripheral.readValue(for: characteristic)
                break
            case CBUUID(string:BLE_UUID_SRV_BATT_CHR_LVL):
                srv_BATT_chr_batt = characteristic
                peripheral.readValue(for: characteristic)
                break
            case CBUUID(string:BLE_UUID_SRV_UART_CHR_TX):
                srv_UART_chr_tx = characteristic
//                /*
//                 Things to send immediately after connection:
//                 1. Unique device ID,
//                 2. Beeping settings
//                 3. Ask for on/off state of brake function
//                 4. Switch on remote battery periodic updates (because it was turned off before)
//                 */
//                if 6 <= LL_Data_Persistent.shared.user_id_logined.count { buf_to_helmet.append(String(LL_Data_Persistent.shared.user_id_logined.prefix(6))) } else { buf_to_helmet.append(String(LL_Data_Persistent.shared.user_name_logined.prefix(6))) }
//                if let beeping = LL_CMD_BEEPING_SETTINGS[beeping_setting] { buf_to_helmet.append(beeping) }
//                buf_to_helmet.append(LL_CMD_GET_BRAKE_FUNCTION_STATE)
//                buf_to_helmet.append(LL_CMD_SWITCH_ON_REMOTE_BATTERY_NOTIFICATIONS)
                buf_to_helmet.append("TESTT")
                break
            case CBUUID(string:BLE_UUID_SRV_UART_CHR_RX):
                srv_UART_chr_rx = characteristic
                peripheral.setNotifyValue(true, for: characteristic)
                break
            default: break
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        guard nil == error else { print("LL_ERR: didUpdateValueFor  \(String(describing: error?.localizedDescription))"); return; }
        
        switch characteristic.uuid {
        case CBUUID(string:BLE_UUID_SRV_DIS_CHR_VER):
//            if let firmwareRevision = characteristic.value?.utf8 { //utf8
//                firmware_version = firmwareRevision.substring(from: firmwareRevision.index(firmwareRevision.startIndex, offsetBy: 1))
//            }
            // how to launch DFU-checking?
            break
        case CBUUID(string:BLE_UUID_SRV_BATT_CHR_LVL):
//            if let value = characteristic.value {
//                battery_percentage_helmet = Double(round_battery_value_to_1to100(value.u8)) / 100.0
//            }
            break
        case CBUUID(string:BLE_UUID_SRV_UART_CHR_TX):
            // nothing to do
            break
        case CBUUID(string:BLE_UUID_SRV_UART_CHR_RX):
            if let rxValue = characteristic.value {
                if let str_data = String(data: rxValue as Data, encoding: String.Encoding.utf8) {
                    print(str_data)
                } else {
                    for u8 in rxValue.u8s {
                        print(u8.description)
                    }
                }
                parse(rxValue)
            }
            break
        default:
            // nothing to do
            break
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        is_tx_busy = false
        guard nil == error else { print("LL_ERR: didWriteValueFor \(String(describing: error?.localizedDescription))"); return; }
        if 0 != buf_to_helmet.count { buf_to_helmet.removeFirst() }
    }
    
}

//extension LL_BlePeripheral_Helmet : DFUServiceDelegate, DFUProgressDelegate {
//    func dfuStateDidChange(to state: DFUState) { print("DFU dfuStateDidChange: \(state.description())");
//        dfu_state = state
//        switch state {
//        case .connecting:      break;
//        case .starting:        break;
//        case .enablingDfuMode: break;
//        case .uploading:       break;
//        case .validating:      break;
//        case .disconnecting:   break;
//        case .completed:       LL_BleCentral_ForHelmet.shared.recover_after_DFU(); break;
//        case .aborted:         LL_BleCentral_ForHelmet.shared.recover_after_DFU(); break;
//        }
//    }
//    
//    func dfuError(_ error: DFUError, didOccurWithMessage message: String) { print("DFU dfuError: \(error) \(message)")
////      dfu_progress = 0 // back to 0 if error
//    }
//    
//    func dfuProgressDidChange(for part: Int, outOf totalParts: Int, to progress: Int, currentSpeedBytesPerSecond: Double, avgSpeedBytesPerSecond: Double) { print("DFU dfuProgressDidChange: \(progress)");
//        dfu_progress = progress
//    }
//}



