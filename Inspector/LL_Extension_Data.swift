//
//  NSData+Lumos.swift
//  Lumos
//
//  Created by Suet Yi Lee on 5/20/16.
//  Copyright © 2016 Suet Yi Lee. All rights reserved.
//
// See http://stackoverflow.com/questions/32894363/reading-a-ble-peripheral-characteristic-and-checking-its-value?rq=1

import Foundation

extension Data {
    var u8:UInt8 {
        assert(self.count >= 1)
        var byte:UInt8 = 0x00
        (self as NSData).getBytes(&byte, length: 1)
        return byte
    }
    
    var u16:UInt16 {
        assert(self.count >= 2)
        var word:UInt16 = 0x0000
        (self as NSData).getBytes(&word, length: 2)
        return word
    }
    
    var u32:UInt32 {
        assert(self.count >= 4)
        var u32:UInt32 = 0x00000000
        (self as NSData).getBytes(&u32, length: 4)
        return u32
    }
    
    var u8s:[UInt8] { // Array of UInt8, Swift byte array basically
        var buffer:[UInt8] = [UInt8](repeating: 0, count: self.count)
        (self as NSData).getBytes(&buffer, length: self.count)
        return buffer
    }
    
    var utf8:String? {
        return String(data: self, encoding: String.Encoding.utf8)
    }
}
