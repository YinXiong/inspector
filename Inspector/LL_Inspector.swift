//  Copyright © 2017 Lumen Labs. All rights reserved.



import Foundation



// Usage:
//          (None)
//
// Remarks:
//          (None)
//
class LL_Inspector: NSObject { static let shared = LL_Inspector(); private override init() {}
    public var sn = ""
//  public var sn_from_scaner = ""
//  public var sn_from_text_input = ""
    public var rssi = Int(0)
    public var batt_helmet = -1
    public var batt_remote = -1
    public var firmware_version = ""

    
    
    public enum E_STATE_of_LL_Inspector: Int {
        case INIT = 0
        case WAIT_SN
        case WAIT_HELMET
        case HELMET_CONNECTED
    }
    public var state = E_STATE_of_LL_Inspector.INIT
    fileprivate func state_mathine() {
        switch state {
        case .INIT:
            // initialize
            sn = ""
            rssi = Int(0)
            batt_helmet = -1
            batt_remote = -1
            firmware_version = ""
            // change state
            state = .WAIT_SN
        case .WAIT_SN:
            if sn != "" { // sn is ok either by scaner or user-input
                // reset BLE
                for helmet in LL_BleCentral_ForHelmet.shared.helmets_connected {
                    LL_BleCentral_ForHelmet.shared.central_manager.cancelPeripheralConnection(helmet.peripheral)
                }; LL_BleCentral_ForHelmet.shared.helmets_connected.removeAll()
                LL_BleCentral_ForHelmet.shared.scan()
                // change state
                state = .WAIT_HELMET
            }
        case .WAIT_HELMET:
            if LL_BleCentral_ForHelmet.shared.helmets_connected.count > 0 { // helmet connected
                // change state
                state = .HELMET_CONNECTED
            }
        case .HELMET_CONNECTED:
            if LL_BleCentral_ForHelmet.shared.helmets_connected.count == 0 { // helmet disconnected
                // change state
                state = .INIT
            }
        }
    }
    
    public func mainloop() {
        state_mathine()
    }
}


