//  Copyright © 2017 Lumen Labs. All rights reserved.



enum DEVICE_TYPE {
    case UNKNOWN
    case HELMET_KICKSTARTER
    case HELMET_LITE
    case REMOTE
};

/*
// Turn Beep Command sent to helmet
let TURN_BEEP_CMD_LEN       = 2
let TURN_BEEP_CMD_MINIMAL   = "SO"
let TURN_BEEP_CMD_LESS_FREQ = "SL"
let TURN_BEEP_CMD_MORE_FREQ = "SH"
let TURN_BEEP_CMD_CONSTANT  = "SC"
*/


// The advertising name should be "LumosHelmet" or "LumosLite" or "LumosRmt"(remote).
let BLE_ADV_NAME_OF_HELMET_LUMOS = "LumosHelmet"
let BLE_ADV_NAME_OF_HELMET_LITE  = "LumosLite"
// The advertising data should contain manufacturer data, of which the 1st byte indicate its pairing status:
let HELMET_PAIRING_STATUS_NO  : UInt8 = 0
let HELMET_PAIRING_STATUS_YES : UInt8 = 1
// use BLE UART service
let BLE_UUID_SRV_UART        = "6E400001-B5A3-F393-E0A9-E50E24DCCA9E"
let BLE_UUID_SRV_UART_CHR_TX = "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
let BLE_UUID_SRV_UART_CHR_RX = "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"
// use BLE DIS service
let BLE_UUID_SRV_DIS         = "180A"
let BLE_UUID_SRV_DIS_CHR_VER = "2A26"
// The advertising data should contain this UUID:
let BLE_UUID_SRV_BATT         = "180F"
let BLE_UUID_SRV_BATT_CHR_LVL = "2A19"
// cmd to helmet
let LL_REMOTE_BATTERY_NOT_AVAILABLE = "NA"
let LL_CMD_REQUEST_REMOTE_BATTERY_LEVEL = "BL-R"
let LL_CMD_RESPONSE_REMOTE_BATTERY_LEVEL = "BL-R:"
let LL_CMD_REQUEST_HELMET_BATTERY_LEVEL = "BL"
let LL_CMD_RESPONSE_HELMET_BATTERY_LEVEL = "BL:"
let LL_CMD_SWITCH_ON_REMOTE_BATTERY_NOTIFICATIONS = "BLO"
let LL_CMD_SWITCH_OFF_REMOTE_BATTERY_NOTIFICATIONS = "BLC"
let LL_CMD_SWITCH_ON_BRAKE_FUNCTION = "BO"
let LL_CMD_SWITCH_OFF_BRAKE_FUNCTION = "BC"
let LL_CMD_GET_BRAKE_FUNCTION_STATE = "BS"
let LL_CMD_BEEPING_SETTINGS: [Int: String] = [0: "SN", 1: "SO", 2: "SL", 3: "SH", 4: "SC"]
let LL_CMD_DISCONNECTING_HEADS_UP = "DSC"
enum LL_BatteryNotificationSetting: Int { case notifyOnConnect = 0, notifyOnDisconnect, notifyOnBoth, notifyOnBothWhenLow, doNotNotify }



let BLE_ADV_NAME_OF_REMOTE = "LumosRmt"
let BLE_UUID_SRV_DFU = "00001530-1212-EFDE-1523-785FEABCD123"
