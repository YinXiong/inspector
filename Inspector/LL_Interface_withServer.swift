////  Copyright © 2017 Lumen Labs. All rights reserved.
//
//
//
//import Foundation
//import CoreLocation // CLLocationCoordinate2D
//
//
//
//////////////////////// account register //////////////////////
//// server will return: ("data" of "completionHandler" of URLSession.dataTask)
//// {"code":200,"message":"User data storage is successful","data":[]}
//// {"code":4000,"message":"User  exist","data":[]}, and user type is Facebook/Google
//// {"code":4001,"message":"User  exist","data":[]}
//let REGISTER_URL =  "https://app.lumoshelmet.com/user/register"
//func register_request(_ name: String, _ email: String, _ pswd: String, _ dev: String, _ user_type: String) -> URLRequest {
//    let body_string = "email=" + email + "&" + "pwd=" + pswd + "&" + "login_dev=" + dev + "&" + "user_type=" + user_type + "&" + "user_name=" + name
//    var request = URLRequest(url: URL(string: REGISTER_URL)!)
//    request.httpMethod      = "POST"
//    request.httpBody        = body_string.data(using: String.Encoding.utf8)
//    request.timeoutInterval = 15
//    return request
//}
//
//
//
//////////////////////// account login //////////////////////
//// server will return: ("data" of "completionHandler" of URLSession.dataTask)
//// {"code":200,"message":"8","data":[]}
//// {"code":400,"message":"Not find the user","data":[]}
//// {"code":500,"message":"Password error","data":[]}
//let LOGIN_URL  =  "https://app.lumoshelmet.com/user/login"
//func login_request(_ user_name: String, _ pswd: String, _ dev: String) -> URLRequest {
//    let body_string = "email=" + user_name + "&" + "pwd=" + pswd + "&" + "login_dev=" + dev
//    var request = URLRequest(url: URL(string: LOGIN_URL)!)
//    request.httpMethod      = "POST"
//    request.httpBody        = body_string.data(using: String.Encoding.utf8)
//    request.timeoutInterval = 15
//    return request
//}
//
//
//
//////////////////////// account logout //////////////////////
//let LOGOUT_URL =  "https://app.lumoshelmet.com/user/logout"
//func logout_request(_ user_name: String, _ pswd: String) -> URLRequest {
//    let body_string = "email=" + user_name + "&" + "pwd=" + pswd
//    var request = URLRequest(url: URL(string: LOGOUT_URL)!)
//    request.httpMethod      = "POST"
//    request.httpBody        = body_string.data(using: String.Encoding.utf8)
//    request.timeoutInterval = 15
//    return request
//}
//
//
//
//////////////////////// forget password //////////////////////
//let FORGET_PSWD_URL =  "https://app.lumoshelmet.com/user/sendemail"
//func forget_pswd_request(_ email: String) -> URLRequest {
//    var request = URLRequest(url: URL(string: FORGET_PSWD_URL + "?email=" + email)!)
//    request.httpMethod      = "GET"
//    request.timeoutInterval = 20
//    return request
//}
//
//
//
//////////////////////// avatar upload //////////////////////
//let AVATAR_UPLOAD_URL = "https://app.lumoshelmet.com/user/setUserImage"
//func avatar_upload_request(email: String, user_id: String, file_name: String, file_content: Data) -> URLRequest {
//    let dataM = NSMutableData()
//    dataM.append(LL_HTTP.shared.pack_data("email",      email))         //;print(email)
//    dataM.append(LL_HTTP.shared.pack_data("user_id",    user_id))       //;print(user_id)
////  dataM.append(LL_HTTP.shared.pack_data("file",       file_name))
//    dataM.append(LL_HTTP.shared.pack_file_begin(file_name))             //;print(String(data: dataM as Data, encoding: String.Encoding.utf8)!)
//    dataM.append(file_content)
//    dataM.append(LL_HTTP.shared.pack_file_end())
//    
//    var request = URLRequest(url: URL(string: AVATAR_UPLOAD_URL)!)
//    request.setValue("multipart/form-data; boundary=" + LL_HTTP.shared.BOUNDARY, forHTTPHeaderField:"Content-Type")
//    request.httpMethod      = "POST"
//    request.timeoutInterval = 180//15
//    request.httpBody        = dataM as Data
//    return request
//}
//
//////////////////////// avatar download //////////////////////
//let AVATAR_DOWNLOAD_URL = "https://app.lumoshelmet.com/uploads/"//"https://app.lumoshelmet.com/user_img/"
//
//
//
//////////////////////// fitness upload //////////////////////
//// server will return: ("data" of "completionHandler" of URLSession.dataTask)
//// "{\"code\":200,\"message\":\"User data storage is successful\",\"array_data\":[]}"
////            401,  The request method is not correct
////            402,  Checking is not pass
////            403,  Saving file failed
////            404,  User data exist
////            405,  Value null
////            500,  User data storage failed
//let UPLOAD_URL = "https://app.lumoshelmet.com/track/insert_track"
//func fitness_file_name_for_server(_ user_name: String, _ start_time_interval1970: Int) -> String {
//    return user_name + start_time_interval1970.description + ".txt"
//}
//func fitness_file_content_pack(_ trace: STRUCT_TRACE) -> String {
//    var strJson = ""
//    strJson += "{"
//        strJson += "\"arraylist\":["
//            for loc in trace.locations {
//                strJson += String("{\"altitude\":0, \"lat\":\(loc.latitude), \"lng\":\(loc.longitude), \"speed\":0, \"sportState\":0},")
//            }; strJson.remove(at: strJson.index(before: strJson.endIndex)) // delete the last ","
//        strJson += "]"
//    strJson += "}"      ;print("The JSON string is \(strJson)")
//    return strJson
//}
//func fitness_file_content_parse(_ file_content: Data, to trace: inout STRUCT_TRACE) -> Bool {
//    // convert to json
//    guard let json_data = try? JSONSerialization.jsonObject(with: file_content, options: .allowFragments) as? [String: Any] else { return false }
//    // "arraylist"
//    guard let locations = json_data!["arraylist"] as? [[String: Any]] else { return false }
//    trace.locations.removeAll()
//    for loc in locations {
//        guard let lat = loc["lat"] as? CLLocationDegrees else { return false }
//        guard let lng = loc["lng"] as? CLLocationDegrees else { return false }
//        trace.locations.append(CLLocationCoordinate2D(latitude: lat, longitude: lng))
//    }; return true;
//}
//func upload_request(_ email: String, _ trace: STRUCT_TRACE, _ file_name: String, _ file_content: Data) -> URLRequest {
//    let dataM = NSMutableData()
//    dataM.append(LL_HTTP.shared.pack_data("email",      email))
//    dataM.append(LL_HTTP.shared.pack_data("user_id",    trace.user_id))//.description))
//    dataM.append(LL_HTTP.shared.pack_data("ride_name",  LL_DateTime.shared.str_morning_afternoon_evening(trace.date_when_start) + " Ride"))
//    dataM.append(LL_HTTP.shared.pack_data("avg_speed",  LL_Math_DIV(trace.distance, trace.duration).description))
//    dataM.append(LL_HTTP.shared.pack_data("duration",   trace.duration.description))
//    dataM.append(LL_HTTP.shared.pack_data("distance",   trace.distance.description))
//    dataM.append(LL_HTTP.shared.pack_data("start_time", trace.date_when_start.description))
//    dataM.append(LL_HTTP.shared.pack_data("stop_time",  trace.date_when_stop.description))     ;print(String(data: dataM as Data, encoding: String.Encoding.utf8)!)
//    dataM.append(LL_HTTP.shared.pack_file_begin(file_name))
//    dataM.append(file_content)
//    dataM.append(LL_HTTP.shared.pack_file_end())
//
//    var request = URLRequest(url: URL(string: UPLOAD_URL)!)
//    request.setValue("multipart/form-data; boundary=" + LL_HTTP.shared.BOUNDARY, forHTTPHeaderField:"Content-Type")
//    request.httpMethod      = "POST"
//    request.timeoutInterval = 15
//    request.httpBody        = dataM as Data
//    return request
//}
//
//
//
//////////////////////// fitness download //////////////////////
//// download page info: (DISCARD!)
//let DOWNLOAD_PAGE_URL_FIRST = "https://app.lumoshelmet.com/track/select_track"
//let DOWNLOAD_PAGE_URL_NEXT  = "https://app.lumoshelmet.com/track/select_track?page="  // the page number starts from 2!
//// download file list:
//let DOWNLOAD_FILE_LIST_URL  = "https://app.lumoshelmet.com/track/select_all_track"
//// download file:
//let DOWNLOAD_FILE_URL       = "https://app.lumoshelmet.com/uploads/"
//func download_page_request(_ email: String, _ user_id: String, _ page_num: Int) -> URLRequest {
//    var url_string = ""; if(1 == page_num) { url_string = DOWNLOAD_PAGE_URL_FIRST; } else { url_string = DOWNLOAD_PAGE_URL_NEXT + page_num.description; }
//    let body_string = "email=" + email + "&" + "user_id=" + user_id
//    var request = URLRequest(url: URL(string: url_string)!)
//    request.httpMethod      = "POST"
//    request.timeoutInterval = 15
//    request.httpBody        = body_string.data(using: String.Encoding.utf8)
//    return request
//}
//
//
//
//////////////////////// fitness delete //////////////////////
//// server will return: ("data" of "completionHandler" of URLSession.dataTask)
//// "{\"code\":200,\"message\":\"Deleting user data is successful\",\"array_data\":[]}"
//let DELETE_URL = "https://app.lumoshelmet.com/track/delete_track"
//func delete_request(_ email: String, _ user_id: String, _ start_time_interval1970: Int) -> URLRequest {
//    let body_string = "email=" + email + "&" + "user_id=" + user_id + "&" + "start_time=" + start_time_interval1970.description
//    var request = URLRequest(url: URL(string: DELETE_URL)!)
//    request.httpMethod      = "POST"
//    request.timeoutInterval = 15
//    request.httpBody        = body_string.data(using: String.Encoding.utf8)
//    return request
//}
//
//
//
//////////////////////// DFU package download //////////////////////
//let DFU_VERSION_URL = "https://app.lumoshelmet.com/dfu_version"
//// {"versionCode_lumos":"2.6","versionDesc_lumos":"固件升级为2.6,修改了刹车功能","downloadUrl_lumos":"http:app.lumoshelmet.com/helmet.zip","versionCode_lite":"2.5","versionDesc_lite":"固件升级为2.6,修改了刹车功能","downloadUrl_lite":"http://app.lumoshelmet.com/helmet_lite.zip"}
//
