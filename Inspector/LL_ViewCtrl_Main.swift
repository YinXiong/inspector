//  Copyright © 2017 Lumen Labs. All rights reserved.



import UIKit
import AVFoundation

// Usage:
//          (None)
//
// Remarks:
//          (None)
//
class LL_ViewCtrl_Main: UIViewController {
    @IBOutlet weak var txt_serial_number : UITextField!
    @IBAction func txt_serial_number__EditingDidBegin(_ sender: Any) {
        LL_Inspector.shared.state = .INIT
    }
    @IBAction func txt_serial_number__EditingDidEnd(_ sender: Any) {
        if txt_serial_number.text?.count != 14 {
            txt_serial_number.text = "";
            let alertController = UIAlertController(title: "ERROR", message: "The serial number length is not 14!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil); alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        } else {
            LL_Inspector.shared.sn = txt_serial_number.text!
        }
    }
    
    @IBOutlet weak var txt_signal : UITextField!
    @IBOutlet weak var img_result_of_signal: UIImageView!
    @IBOutlet weak var txt_battery_level_of_helmet : UITextField!
    @IBOutlet weak var img_result_of_battery_of_helmet: UIImageView!
    @IBOutlet weak var txt_battery_level_of_remote : UITextField!
    @IBOutlet weak var img_result_of_battery_of_remote: UIImageView!
    @IBOutlet weak var txt_firmware_version : UITextField!
    @IBOutlet weak var img_result_of_firmware_version: UIImageView!
    
    @IBOutlet weak var txtv_status : UITextView!
    
    @IBOutlet weak var btn_scan: UIButton!
    @IBAction func btn_scan_TouchUpInside(_ sender: Any) {
        LL_Inspector.shared.state = .INIT
        performSegue(withIdentifier: "to_scaner", sender: self)
    }
    

    
    // a timer for periodic things:
    fileprivate var timer: Timer?; let TIMER_PERIOD = 1 // unit: s
    @objc func timer_isr() { //print("timer_isr() is called.")
        switch LL_Inspector.shared.state {
        case .INIT:
            // update UI of: Signal / Battery Level / Firmware Version
            txt_signal.text                  = ""; img_result_of_signal.isHidden            = true
            txt_battery_level_of_helmet.text = ""; img_result_of_battery_of_helmet.isHidden = true
            txt_battery_level_of_remote.text = ""; img_result_of_battery_of_remote.isHidden = true
            txt_firmware_version.text        = ""; img_result_of_firmware_version.isHidden  = true
        case .WAIT_SN:
            // update UI of: Signal / Battery Level / Firmware Version
            txt_signal.text                  = ""; img_result_of_signal.isHidden            = true
            txt_battery_level_of_helmet.text = ""; img_result_of_battery_of_helmet.isHidden = true
            txt_battery_level_of_remote.text = ""; img_result_of_battery_of_remote.isHidden = true
            txt_firmware_version.text        = ""; img_result_of_firmware_version.isHidden  = true
        case .WAIT_HELMET:
            // update sn because it maybe input by bar code scaner
            txt_serial_number.text = LL_Inspector.shared.sn
            // update UI of: Signal / Battery Level / Firmware Version
            txt_signal.text                  = ""; img_result_of_signal.isHidden            = true
            txt_battery_level_of_helmet.text = ""; img_result_of_battery_of_helmet.isHidden = true
            txt_battery_level_of_remote.text = ""; img_result_of_battery_of_remote.isHidden = true
            txt_firmware_version.text        = ""; img_result_of_firmware_version.isHidden  = true
        case .HELMET_CONNECTED:
            // update UI of: Signal / Battery Level / Firmware Version
            let signal = LL_Inspector.shared.rssi
            txt_signal.text = signal.description; if signal > -100 { img_result_of_signal.isHidden = false } else { img_result_of_signal.isHidden = true }
            let batt_helmet = LL_Inspector.shared.batt_helmet
            if(batt_helmet > 0) { txt_battery_level_of_helmet.text = batt_helmet.description + "%" }; if batt_helmet > 60 { img_result_of_battery_of_helmet.isHidden = false } else { img_result_of_battery_of_helmet.isHidden = true }
            let batt_remote = LL_Inspector.shared.batt_remote
            if(batt_remote > 0) { txt_battery_level_of_remote.text = batt_remote.description + "%" }; if batt_remote > 60 { img_result_of_battery_of_remote.isHidden = false } else { img_result_of_battery_of_remote.isHidden = true }
            let firmware_version = LL_Inspector.shared.firmware_version
            txt_firmware_version.text = firmware_version; if firmware_version != "" { img_result_of_firmware_version.isHidden  = false } else { img_result_of_firmware_version.isHidden  = true }
        }
    }
    
    // UIViewController
    override func viewDidLoad() { super.viewDidLoad()
        txt_serial_number.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) { super.viewWillAppear(animated); timer = Timer.scheduledTimer(timeInterval: TimeInterval(TIMER_PERIOD), target: self, selector:#selector(timer_isr), userInfo: nil, repeats: true)
    }
    override func viewDidDisappear(_ animated: Bool) { super.viewDidDisappear(animated); timer?.invalidate()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) { // a blank area was touched
        self.view.endEditing(true) // hide keyboard
    }
//    @IBAction func myUnwindAction(unwindSegue: UIStoryboardSegue) {
//        // a func needed for the other view controller to back to this one.
//    }
    
}

extension LL_ViewCtrl_Main: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool { // keyboard "Done" was pressed
        if(textField == txt_serial_number) {
            textField.resignFirstResponder() // hide the keyboard
        }
        return true
    }
}

